TENSORBOARD_PORT=6007
TENSORFLOW_PORT=3001
COMPOSE_PROJECT_NAME=eisamar_cifar
TEMPLATE=docker-compose.yml.jinja
HOST=gpu4
NV_GPU='4'

export TENSORFLOW_PORT
export TENSORBOARD_PORT
export COMPOSE_PROJECT_NAME
export NV_GPU

data: download extract

start:
	NV_GPU='4,5,6' docker-compose up -d
	# NV_GPU='4,5,6' nvidia-docker-compose --template docker/$(TEMPLATE) up -d
	docker logs eisamarcifar_tensorflow_1 2>&1 | grep localhost:8888 | tail -n1 | sed -n 's/localhost:8888/$(HOST):$(TENSORFLOW_PORT)/p' | tr -d '[:space:]'
stop:
	NV_GPU='4,5,6' docker-compose stop
	# NV_GPU='4,5,6' nvidia-docker-compose --template docker/$(TEMPLATE) stop

pull:
	rsync -rav --exclude 'graphs' --exclude 'logs' --exclude 'data' --exclude "exercises/mnist_convnet_model" $(HOST):devel/cifarassignment2018/* .

push:
	rsync -rav --exclude 'graphs' --exclude 'logs' --exclude 'data' --exclude "exercises/mnist_convnet_model" * $(HOST):devel/cifarassignment2018

download:
	mkdir -p data
	wget -P data https://www.cs.toronto.edu/~kriz/cifar-10-python.tar.gz
	wget -P data https://www.cs.toronto.edu/~kriz/cifar-10-binary.tar.gz

extract:
	tar xvzf data/cifar-10-python.tar.gz -C data
	tar xvzf data/cifar-10-binary.tar.gz -C data