"""Runs a ResNet model on the CIFAR-10 dataset."""
# Based on https://github.com/tensorflow/models/tree/master/official/resnet

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import numpy as np
import math
import tensorflow as tf

_HEIGHT = 32
_WIDTH = 32
_NUM_CHANNELS = 3
_DEFAULT_IMAGE_BYTES = _HEIGHT * _WIDTH * _NUM_CHANNELS
# The record is the image plus a one-byte label
_RECORD_BYTES = _DEFAULT_IMAGE_BYTES + 1
_NUM_CLASSES = 10
_NUM_DATA_FILES = 5

_NUM_IMAGES = {
    'train': 50000,
    'validation': 10000,
    'test': 10000
}


###############################################################################
# Data processing
###############################################################################
def get_filenames(is_training, data_dir):
    """Returns a list of filenames."""
    data_dir = os.path.join(data_dir, 'cifar-10-batches-bin')

    assert os.path.exists(data_dir), (
        'Run cifar10_download_and_extract.py first to download and extract the'
        ' CIFAR-10 data.')

    if is_training:
        return [
            os.path.join(data_dir, 'data_batch_%d.bin' % i)
            for i in range(1, _NUM_DATA_FILES + 1)
        ]
    else:
        return [os.path.join(data_dir, 'test_batch.bin')]


def parse_record(raw_record, is_training):
    """Parse CIFAR-10 image and label from a raw record."""
    # Convert bytes to a vector of uint8 that is record_bytes long.
    record_vector = tf.decode_raw(raw_record, tf.uint8)

    # The first byte represents the label, which we convert from uint8 to int32
    # and then to one-hot.
    label = tf.cast(record_vector[0], tf.int32)
    label = tf.one_hot(label, _NUM_CLASSES)

    # The remaining bytes after the label represent the image, which we reshape
    # from [depth * height * width] to [depth, height, width].
    depth_major = tf.reshape(record_vector[1:_RECORD_BYTES],
                             [_NUM_CHANNELS, _HEIGHT, _WIDTH])

    # Convert from [depth, height, width] to [height, width, depth],
    # and cast as float32.
    image = tf.cast(tf.transpose(depth_major, [1, 2, 0]), tf.float32)

    image = preprocess_image(image, is_training)

    return image, label


def preprocess_image(image, is_training):
    """Preprocess a single image of layout [height, width, depth]."""
    if is_training:
        # Resize the image to add four extra pixels on each side.
        image = tf.image.resize_image_with_crop_or_pad(
            image, _HEIGHT + 8, _WIDTH + 8)

        # Random rotate
        angle = 15
        image = tf.contrib.image.rotate(
            image,
            tf.random_uniform([1],
                              maxval=math.pi / 180 * angle,
                              minval=math.pi / 180 * -angle),
            interpolation='BILINEAR')

        # Randomly crop a [_HEIGHT, _WIDTH] section of the image.
        image = tf.random_crop(image, [_HEIGHT, _WIDTH, _NUM_CHANNELS])

        # Randomly flip the image horizontally (with a 50% chance)
        image = tf.image.random_flip_left_right(image)

    # Subtract off the mean and divide by the variance of the pixels.
    image = tf.image.per_image_standardization(image)

    return image


def process_record_dataset(dataset, is_training, batch_size,
                           shuffle_buffer,
                           parse_record_fn, num_epochs=1,
                           num_parallel_calls=1,
                           num_workers=1,
                           worker_index=-1):
    """Given a Dataset with raw records, parse each record into images and labels,
    and return an iterator over the records.
    Args:
      dataset: A Dataset representing raw records
      is_training: A boolean denoting whether the input is for training.
      batch_size: The number of samples per batch.
      shuffle_buffer: The buffer size to use when shuffling records. A larger
        value results in better randomness, but smaller values reduce startup
        time and use less memory.
      parse_record_fn: A function that takes a raw record and returns the
        corresponding (image, label) pair.
      num_epochs: The number of epochs to repeat the dataset.
      num_parallel_calls: The number of records that are processed in parallel.
        This can be optimized per data set but for generally homogeneous data
        sets, should be approximately the number of available CPU cores.
    Returns:
      Dataset of (image, label) pairs ready for iteration.
    """
    # We prefetch a batch at a time, This can help smooth out the time taken to
    # load input files as we go through shuffling and processing.

    dataset = dataset.prefetch(buffer_size=batch_size)

    if is_training:
        if worker_index >= 0:
            dataset = dataset.shard(num_workers, worker_index)
        # Shuffle the records. Note that we shuffle before repeating to ensure
        # that the shuffling respects epoch boundaries.
        dataset = dataset.shuffle(buffer_size=shuffle_buffer)

    # If we are training over multiple epochs before evaluating, repeat the
    # dataset for the appropriate number of epochs.
    dataset = dataset.repeat(num_epochs)

    # Parse the raw records into images and labels
    dataset = dataset.map(lambda value: parse_record_fn(value, is_training),
                          num_parallel_calls=num_parallel_calls)

    dataset = dataset.batch(batch_size)

    # Operations between the final prefetch and the
    # get_next call to the iterator
    # will happen synchronously during run time. We prefetch here again to
    # background all of the above processing work and keep it out of the
    # critical training path.
    dataset = dataset.prefetch(1)

    # iterator = dataset.make_initializable_iterator()
    # return dataset.make_initializable_iterator().get_next()
    return dataset.make_one_shot_iterator().get_next()


def get_test_labels(data_dir):
    """Return the test set."""
    filenames = get_filenames(False, data_dir)
    parse_record_fn = parse_record
    dataset = tf.data.FixedLengthRecordDataset(filenames, _RECORD_BYTES)
    dataset = dataset.map(lambda value: parse_record_fn(value, False),
                          num_parallel_calls=1)
    np_labels = np.asarray(dataset.labels, dtype=np.int32)
    return np_labels

def input_fn(is_training, data_dir, batch_size, num_epochs=1,
             num_parallel_calls=32,
             num_workers=1,
             worker_index=-1):
    """Input_fn using the tf.data input pipeline for CIFAR-10 dataset.

    Args:
      is_training: A boolean denoting whether the input is for training.
      data_dir: The directory containing the input data.
      batch_size: The number of samples per batch.
      num_epochs: The number of epochs to repeat the dataset.
      num_parallel_calls: The number of records that are processed in parallel.
        This can be optimized per data set but for generally homogeneous data
        sets, should be approximately the number of available CPU cores.

    Returns:
      A dataset that can be used for iteration.
    """
    filenames = get_filenames(is_training, data_dir)
    dataset = tf.data.FixedLengthRecordDataset(filenames, _RECORD_BYTES)

    def training_data():
        with tf.name_scope('Training_data'):

            # So this gives test data ... no validation data.
            no_ex = _NUM_IMAGES['test']

            if is_training:
                no_ex = _NUM_IMAGES['train']

            return process_record_dataset(
                dataset,
                is_training,
                batch_size,
                no_ex,
                parse_record,
                num_epochs,
                num_parallel_calls,
                num_workers,
                worker_index)

    return training_data
