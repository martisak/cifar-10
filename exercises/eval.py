import tensorflow as tf
import cifar_data
import cifar10_read
import cnn_models_old
import cnn_models
import os, glob
import numpy as np
import util

def model_fn(features, labels, mode, params):

    if params.get('model','new') == "old":
        y = cnn_models_old.vgg16(features, mode, params)
    else:
        y = cnn_models.vgg16(features, mode, params)

    y_ = labels

    with tf.variable_scope("evaluation"):
        with tf.variable_scope("prediction"):
            # scores = tf.nn.softmax(y, name='softmax_tensor')
            pred = tf.argmax(y, 1)
            # prediction = tf.reshape(y, [-1])

    predictions = {
        'results': pred,
        'probabilities': tf.nn.softmax(y, name="softmax_tensor"),
        'logits': y,
    }

    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(
            mode=mode,
            predictions=predictions)

    with tf.variable_scope("cross_entropy"):
        cross_entropy = tf.nn.softmax_cross_entropy_with_logits_v2(
            labels=y_,
            logits=y)

        loss = tf.reduce_mean(cross_entropy, name='xentropy_mean') + \
            tf.losses.get_regularization_loss()

    with tf.variable_scope("accuracy"):
        accuracy_op = tf.metrics.accuracy(
            labels=tf.argmax(y_, 1), predictions=pred)
        eval_metric = {'accuracy': accuracy_op}

        # correct_prediction = tf.equal(pred, tf.argmax(y_, 1))
        # accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    eval_metric = {'accuracy': accuracy_op}

    # summary_hook = tf.train.SummarySaverHook(
    #     save_steps=10,
    #     summary_op=tf.summary.merge_all())

    if mode == tf.estimator.ModeKeys.EVAL:

        return tf.estimator.EstimatorSpec(
            mode=mode,
            loss=loss,
            # predictions=predictions,
            eval_metric_ops=eval_metric)  # ,
        #    evaluation_hooks=[summary_hook])

    if mode == tf.estimator.ModeKeys.TRAIN:

        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):

            # Horovod: scale learning rate by the number of workers.

            train_op = tf.contrib.layers.optimize_loss(
                loss=loss,
                global_step=tf.train.get_global_step(),
                optimizer=tf.train.AdamOptimizer,
                learning_rate=10**params.learning_rate,  # * hvd.size(),
                learning_rate_decay_fn=my_decay
            )

        # optimizer = tf.train.AdamOptimizer(
        #     learning_rate=0.001 * hvd.size())

        # # Horovod: add Horovod Distributed Optimizer.
        # optimizer = hvd.DistributedOptimizer(optimizer)

        # train_op = optimizer.minimize(
        #     loss=loss,
        #     global_step=tf.train.get_global_step())

        # hooks = [summary_hook]  # if hvd.rank() == 0 else None

        return tf.estimator.EstimatorSpec(
            mode=mode,
            loss=loss,
            train_op=train_op)  # ,
        # training_hooks=hooks)
    else:
        train_op = None


logger = util.get_logger("eval")

event_paths = []

# logging_dir = '../logs/network6/'
# # path = os.path.join(logging_dir, "_*")
# # event_paths = event_paths + glob.glob(path)

# path = os.path.join(logging_dir, "ensemble_*","*")
# event_paths = event_paths + glob.glob(path)

logging_dir = '../logs/network6/hyper/'

path = os.path.join(logging_dir, "_*")
event_paths = event_paths + glob.glob(path)

path = os.path.join(logging_dir, "ensemble_*","*")
event_paths = event_paths + glob.glob(path)



logging_dir = '../logs/network6/ensemble/'

path = os.path.join(logging_dir, "_*")
event_paths = event_paths + glob.glob(path)

path = os.path.join(logging_dir, "ensemble_*","*")
event_paths = event_paths + glob.glob(path)



logging_dir = '../logs/network6/leakyrelu_large/'

path = os.path.join(logging_dir, "_*")
event_paths = event_paths + glob.glob(path)

path = os.path.join(logging_dir, "ensemble_*","*")
event_paths = event_paths + glob.glob(path)


predictions = np.empty((0, 10000), int)

a = []

for i, event in enumerate(event_paths):

    logger.info("Evaluating model number {} from {}".format(i, event))

    model_path=event


    if "ensemble" in event:
        if "leaky" in event:
            p = "_-3.0_0.9_64.0_-6.0/.".split('_')
        elif "hyper" in event:
            p = "_-2.0_0.808070726115_368.0_-5.0/.".split('_')
        else:
            p = "_-2.0_0.8_384.0_-5.0/.".split('_')
    else:
        p = model_path.replace("leakyrelu_large", "leakyrelularge").split('_')

    logger.debug(p)
    learning_rate = float(p[1])
    keep_prob = float(p[2])
    hidden_units = float(p[3])
    lmbda = float(p[4].split('/')[0])

    no_parallel_calls = 48
    data_dir = '/notebooks/data/'
    batchsize = 128

    sess_conf = tf.ConfigProto(
          allow_soft_placement=True,
          log_device_placement=False
            )

    # sess_conf.gpu_options.per_process_gpu_memory_fraction = 0.4
    # sess_conf.gpu_options.allow_growth = False

    run_config = tf.estimator.RunConfig(
        model_dir=model_path,
        session_config=sess_conf)

    #eval_input_fn = cifar_data.input_fn(
    #        False, data_dir, batchsize, num_epochs=1,
    #        num_parallel_calls=no_parallel_calls,
    #    )
    model = "old"

    if "leaky" in event:
        model = "new"

    hparams = tf.contrib.training.HParams(
        dropout=True,
        keep_prob=keep_prob,
        learning_rate=learning_rate,
        hidden_units=hidden_units,
        lmbda=lmbda,
        model=model)

    # Start at 0 seconds, evaluate every 2 min
    #eval_spec = tf.estimator.EvalSpec(
    #    input_fn=eval_input_fn,
    #    start_delay_secs=0,
    #    throttle_secs=120)

    test_input_fn = cifar_data.input_fn(
        False, data_dir, batchsize, num_epochs=1,
        num_parallel_calls=no_parallel_calls)

    estimator = tf.estimator.Estimator(
        model_fn=model_fn,  # First-class function
        params=hparams,  # HParams
        config=run_config  # RunConfig
    )

    acc = estimator.evaluate(input_fn=test_input_fn)["accuracy"]
    a.append(acc)

    logger.debug("Accuracy on {} was {}".format(event, acc))

    p = list(estimator.predict(input_fn=test_input_fn))
    predictions = np.vstack([predictions, [result['results'] for result in p]])

# This is bloody stupid, should read test set labels
# from cifar_data instead.
test_fname = "../data/cifar-10-batches-py/test_batch"
test_images, labels = cifar10_read.read_cifar10(
    test_fname, one_hot=False)

predictions.astype(int)

filt_preds = np.column_stack([np.array(a), predictions])
axis = 0

logger.debug(filt_preds)

np.save("predictions.npy", filt_preds)