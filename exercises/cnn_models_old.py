import util
import tensorflow as tf


def vgg16(features, mode, params):
    """
    VGG16-like model inspired by for example
    https://github.com/geifmany/cifar-vgg
    """

    logger = util.get_logger("cnn_net")

    logger.debug("Model parameters: {}".format(util.ppjson(params)))

    is_training = mode == tf.estimator.ModeKeys.TRAIN

    lmbda = 10**params.lmbda
    # dropout = params.dropout

    if is_training:
        keep_prob = params.keep_prob
    else:
        keep_prob = 1.0

    with tf.variable_scope("input"):

        rawimage = tf.cast(features, tf.float32)
        image = tf.reshape(rawimage, [-1, 32, 32, 3])

        # tf.summary.image('input', image, 1)

    with tf.variable_scope("layer1"):
        W1 = tf.get_variable('w1', [3, 3, 3, 64],
                             initializer=tf.keras.initializers.he_normal(),
                             regularizer=tf.contrib.layers.l2_regularizer(scale=lmbda))

        # Actually DropConnect
        # if dropout:
        #    W1 = tf.nn.dropout(W1, keep_prob) * keep_prob

        y1_ = tf.nn.relu(tf.nn.conv2d(image, W1,
                                      strides=[1, 1, 1, 1], padding='SAME'))

        y1 = tf.contrib.layers.batch_norm(y1_,
                                          scale=True,
                                          is_training=is_training,
                                          scope='bn')

    with tf.variable_scope("layer2"):
        W2 = tf.get_variable('w2', [3, 3, 64, 64],
                             initializer=tf.keras.initializers.he_normal(),
                             regularizer=tf.contrib.layers.l2_regularizer(scale=lmbda))

        # if dropout:
        #    W2 = tf.nn.dropout(W2, keep_prob) * keep_prob

        y2_ = tf.nn.relu(tf.nn.conv2d(y1, W2,
                                      strides=[1, 1, 1, 1], padding='SAME'))

        pool2_ = tf.nn.max_pool(y2_,
                                ksize=[1, 2, 2, 1],
                                strides=[1, 2, 2, 1], padding="SAME")

        pool2 = tf.contrib.layers.batch_norm(pool2_,
                                             scale=True,
                                             is_training=is_training,
                                             scope='bn')

        # 16x16

    with tf.variable_scope("layer3"):
        W3 = tf.get_variable('w3', [3, 3, 64, 128],
                             initializer=tf.keras.initializers.he_normal(),
                             regularizer=tf.contrib.layers.l2_regularizer(scale=lmbda))

        # if dropout:
        #    W3 = tf.nn.dropout(W3, keep_prob) * keep_prob

        y3_ = tf.nn.relu(tf.nn.conv2d(pool2, W3,
                                      strides=[1, 1, 1, 1], padding='SAME'))

        y3 = tf.contrib.layers.batch_norm(y3_,
                                          scale=True,
                                          is_training=is_training,
                                          scope='bn')
        do3 = tf.layers.dropout(y3, rate=1 - keep_prob, training=is_training)

    with tf.variable_scope("layer4"):

        W4 = tf.get_variable('w4', [3, 3, 128, 128],
                             initializer=tf.keras.initializers.he_normal(),
                             regularizer=tf.contrib.layers.l2_regularizer(scale=lmbda))

        # if dropout:
        #    W4 = tf.nn.dropout(W4, keep_prob) * keep_prob

        y4_ = tf.nn.relu(tf.nn.conv2d(do3, W4,
                                      strides=[1, 1, 1, 1], padding='SAME'))

        do4 = tf.layers.dropout(y4_, rate=1 - keep_prob, training=is_training)

        pool4_ = tf.nn.max_pool(do4,
                                ksize=[1, 2, 2, 1],
                                strides=[1, 2, 2, 1], padding="SAME")

        pool4 = tf.contrib.layers.batch_norm(pool4_,
                                             scale=True,
                                             is_training=is_training,
                                             scope='bn')

        # tf.summary.histogram('w4', W4)
        # 16*16

    with tf.variable_scope("layer5"):
        W5 = tf.get_variable('w5', [3, 3, 128, 256],
                             initializer=tf.keras.initializers.he_normal(),
                             regularizer=tf.contrib.layers.l2_regularizer(scale=lmbda))

        # if dropout:
        #    W5 = tf.nn.dropout(W5, keep_prob) * keep_prob

        y5 = tf.nn.relu(tf.nn.conv2d(pool4, W5,
                                     strides=[1, 1, 1, 1], padding='SAME'))

        do5 = tf.layers.dropout(y5, rate=1 - keep_prob, training=is_training)

    with tf.variable_scope("layer6"):
        W6 = tf.get_variable('w6', [3, 3, 256, 256],
                             initializer=tf.keras.initializers.he_normal(),
                             regularizer=tf.contrib.layers.l2_regularizer(scale=lmbda))

        # if dropout:
        #     W6 = tf.nn.dropout(W6, keep_prob) * keep_prob

        # if dropout:
        #    W6 = tf.nn.dropout(W6, keep_prob) * keep_prob

        y6_ = tf.nn.relu(tf.nn.conv2d(
            do5, W6, strides=[1, 1, 1, 1], padding='SAME'))

        do6 = tf.layers.dropout(y6_, rate=1 - keep_prob, training=is_training)

        pool6_ = tf.nn.max_pool(do6, ksize=[1, 2, 2, 1], strides=[
                                1, 2, 2, 1], padding="SAME")

        pool6 = tf.contrib.layers.batch_norm(
            pool6_, scale=True, is_training=is_training, scope='bn')

        # tf.summary.histogram('w6', W6)
        # 8x8

    with tf.variable_scope("layer7-1"):
        W71 = tf.get_variable(
            'w71', [3, 3, 256, 512],
            initializer=tf.keras.initializers.he_normal(),
            regularizer=tf.contrib.layers.l2_regularizer(scale=lmbda))

        # if dropout:
        #     W71 = tf.nn.dropout(W71, keep_prob) * keep_prob

        y71 = tf.nn.relu(tf.nn.conv2d(
            pool6, W71, strides=[1, 1, 1, 1], padding='SAME'))

        do71 = tf.layers.dropout(y71, rate=1 - keep_prob, training=is_training)

    with tf.variable_scope("layer7-2"):
        W72 = tf.get_variable(
            'w72', [3, 3, 512, 512],
            initializer=tf.keras.initializers.he_normal(),
            regularizer=tf.contrib.layers.l2_regularizer(scale=lmbda))

        # if dropout:
        #     W72 = tf.nn.dropout(W72, keep_prob) * keep_prob

        y72 = tf.nn.relu(tf.nn.conv2d(
            do71, W72, strides=[1, 1, 1, 1], padding='SAME'))

        do72 = tf.layers.dropout(y72, rate=1 - keep_prob, training=is_training)

    with tf.variable_scope("layer7-3"):
        W73 = tf.get_variable(
            'w73', [3, 3, 512, 512],
            initializer=tf.keras.initializers.he_normal(),
            regularizer=tf.contrib.layers.l2_regularizer(scale=lmbda))

        y73_ = tf.nn.relu(tf.nn.conv2d(
            do72, W73, strides=[1, 1, 1, 1], padding='SAME'))

        do73 = tf.layers.dropout(y73_,
                                 rate=1 - keep_prob,
                                 training=is_training)

        pool73_ = tf.nn.max_pool(do73, ksize=[1, 2, 2, 1], strides=[
            1, 2, 2, 1], padding="SAME")

        pool73 = tf.contrib.layers.batch_norm(
            pool73_, scale=True, is_training=is_training, scope='bn')

        # tf.summary.histogram('w73', W73)

    # with tf.variable_scope("layer8-1"):
    #     W81 = tf.get_variable(
    #         'w81', [3, 3, 512, 512],
    #         initializer=tf.keras.initializers.he_normal(),
    #         regularizer=tf.contrib.layers.l2_regularizer(scale=lmbda))
    #     # if dropout:
    #     #     W81 = tf.nn.dropout(W81, keep_prob) * keep_prob

    #     y81 = tf.nn.relu(tf.nn.conv2d(
    #         pool73, W81, strides=[1, 1, 1, 1], padding='SAME'))

    #     do81 = tf.layers.dropout(y81,
    #                              rate=1 - keep_prob,
    #                              training=is_training)

    # with tf.variable_scope("layer8-2"):
    #     W82 = tf.get_variable(
    #         'w82', [3, 3, 512, 512],
    #         initializer=tf.keras.initializers.he_normal(),
    #         regularizer=tf.contrib.layers.l2_regularizer(scale=lmbda))
    #     # if dropout:
    #     #     W82 = tf.nn.dropout(W82, keep_prob) * keep_prob

    #     y82 = tf.nn.relu(tf.nn.conv2d(
    #         y81, W82, strides=[1, 1, 1, 1], padding='SAME'))

    # with tf.variable_scope("layer8-3"):
    #     W83 = tf.get_variable(
    #         'w83', [3, 3, 512, 512],
    #         initializer=tf.keras.initializers.he_normal(),
    #         regularizer=tf.contrib.layers.l2_regularizer(scale=lmbda))

    #     y83_ = tf.nn.relu(tf.nn.conv2d(
    #         y82, W83, strides=[1, 1, 1, 1], padding='SAME'))

    #     pool83_ = tf.nn.max_pool(y83_, ksize=[1, 2, 2, 1], strides=[
    #         1, 2, 2, 1], padding="SAME")

    #     pool83 = tf.contrib.layers.batch_norm(
    #         pool83_, scale=True, is_training=is_training, scope='bn')

        # tf.summary.histogram('w83', W83)

    with tf.variable_scope("layer9"):
        y8_flat = tf.reshape(pool73, [-1, 2 * 2 * 512])

        W9 = tf.get_variable('w9', [2 * 2 * 512, params.hidden_units],
                             initializer=tf.keras.initializers.he_normal(),
                             regularizer=tf.contrib.layers.l2_regularizer(scale=lmbda))

        b9 = tf.get_variable(
            'b9', [params.hidden_units],
            initializer=tf.constant_initializer(0.1))

        # Actually DropConnect
        # if dropout:
        #     W9 = tf.nn.dropout(W9, keep_prob) * keep_prob
        #     b9 = tf.nn.dropout(b9, keep_prob) * keep_prob

        y9_ = tf.nn.relu(tf.matmul(y8_flat, W9) + b9)

        y9 = tf.contrib.layers.batch_norm(
            y9_, scale=True, is_training=is_training, scope='bn')

        fc1 = tf.layers.dropout(y9, rate=1 - keep_prob, training=is_training)

    with tf.variable_scope("output"):
        W10 = tf.get_variable(
            'w10', [params.hidden_units, 10],
            initializer=tf.keras.initializers.he_normal(),
            regularizer=tf.contrib.layers.l2_regularizer(scale=lmbda))
        b10 = tf.get_variable(
            'b10', [10], initializer=tf.constant_initializer(0.1))
        y10 = tf.matmul(fc1, W10) + b10

    y = y10

    return y


def all_cnn_c(features, mode, params):
    """
    STRIVING FOR SIMPLICITY: THE ALL CONVOLUTIONAL NET
    ICLR 2015
    Jost Tobias Springenberg, Alexey Dosovitskiy
    Thomas Brox, Martin Riedmiller
    """

    logger = util.get_logger("all-cnn-c")

    logger.debug("Model parameters: {}".format(util.ppjson(params)))

    is_training = mode == tf.estimator.ModeKeys.TRAIN

    lmbda = 10**params.lmbda

    if is_training:
        keep_prob = 1 - params.keep_prob
    else:
        keep_prob = 1 - 1.0

    with tf.variable_scope("input"):

        rawimage = tf.cast(features, tf.float32)
        image = tf.reshape(rawimage, [-1, 32, 32, 3])

        # tf.summary.image('input', image, 1)
        # image = tf.layers.dropout(image_,
        #                         rate=1-keep_prob,
        #                         training=is_training)

    with tf.variable_scope("layer1"):

        conv1_ = tf.layers.conv2d(
            inputs=image,
            filters=96,
            strides=(1, 1),
            kernel_size=[3, 3],
            kernel_regularizer=tf.contrib.layers.l2_regularizer(scale=lmbda),
            padding="same",
            activation=tf.nn.relu,
            kernel_initializer=tf.contrib.layers.xavier_initializer())

        conv1 = tf.layers.batch_normalization(conv1_, training=is_training)

    with tf.variable_scope("layer2"):
        conv2_ = tf.layers.conv2d(
            inputs=conv1,
            filters=96,
            strides=(1, 1),
            kernel_size=[3, 3],
            kernel_regularizer=tf.contrib.layers.l2_regularizer(scale=lmbda),
            padding="same",
            activation=tf.nn.relu,
            kernel_initializer=tf.contrib.layers.xavier_initializer())

        conv2 = tf.layers.batch_normalization(conv2_, training=is_training)

    with tf.variable_scope("layer3"):
        conv3_ = tf.layers.conv2d(
            inputs=conv2,
            filters=96,
            strides=(2, 2),
            kernel_size=[3, 3],
            kernel_regularizer=tf.contrib.layers.l2_regularizer(scale=lmbda),
            padding="same",
            activation=tf.nn.relu,
            kernel_initializer=tf.contrib.layers.xavier_initializer())

        conv3_do = tf.layers.dropout(
            conv3_, rate=1 - keep_prob, training=is_training)

        conv3 = tf.layers.batch_normalization(conv3_do, training=is_training)

    with tf.variable_scope("layer4"):
        conv4_ = tf.layers.conv2d(
            inputs=conv3,
            filters=192,
            strides=(1, 1),
            kernel_size=[3, 3],
            kernel_regularizer=tf.contrib.layers.l2_regularizer(scale=lmbda),
            padding="same",
            activation=tf.nn.relu,
            kernel_initializer=tf.contrib.layers.xavier_initializer())

        conv4 = tf.layers.batch_normalization(conv4_, training=is_training)

    with tf.variable_scope("layer5"):
        conv5_ = tf.layers.conv2d(
            inputs=conv4,
            filters=192,
            strides=(1, 1),
            kernel_size=[3, 3],
            kernel_regularizer=tf.contrib.layers.l2_regularizer(scale=lmbda),
            padding="valid",
            activation=tf.nn.relu,
            kernel_initializer=tf.contrib.layers.xavier_initializer())

        conv5 = tf.layers.batch_normalization(conv5_, training=is_training)

    with tf.variable_scope("layer6"):
        conv6_ = tf.layers.conv2d(
            inputs=conv5,
            filters=192,
            strides=(2, 2),
            kernel_size=[3, 3],
            kernel_regularizer=tf.contrib.layers.l2_regularizer(scale=lmbda),
            padding="same",
            activation=tf.nn.relu,
            kernel_initializer=tf.contrib.layers.xavier_initializer())

        conv6_do = tf.layers.dropout(
            conv6_, rate=1 - keep_prob, training=is_training)

        conv6 = tf.layers.batch_normalization(conv6_do, training=is_training)

    with tf.variable_scope("layer7"):
        conv7_ = tf.layers.conv2d(
            inputs=conv6,
            filters=192,
            strides=(1, 1),
            kernel_size=[1, 1],
            kernel_regularizer=tf.contrib.layers.l2_regularizer(scale=lmbda),
            padding="same",
            activation=tf.nn.relu,
            kernel_initializer=tf.contrib.layers.xavier_initializer())

        conv7_do = tf.layers.dropout(
            conv7_, rate=1 - keep_prob, training=is_training)

        conv7 = tf.layers.batch_normalization(conv7_, training=is_training)

    with tf.variable_scope("layer8"):
        conv8_ = tf.layers.conv2d(
            inputs=conv7,
            filters=10,
            strides=(1, 1),
            kernel_size=[1, 1],
            kernel_regularizer=tf.contrib.layers.l2_regularizer(scale=lmbda),
            padding="valid",
            activation=tf.nn.relu,
            kernel_initializer=tf.contrib.layers.xavier_initializer())

        conv8 = tf.nn.avg_pool(conv8_, ksize=[1, 6, 6, 1],
                               strides=[1, 6, 6, 1], padding="VALID")

    with tf.variable_scope("output"):
        conv9_flat = tf.reshape(conv8, [-1, 10])
        y = tf.layers.dense(inputs=conv9_flat, units=10)

    return y
