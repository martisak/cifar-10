# simple python script to train a 1-layer neural network to classify cifar10
# images use the tensorflow library

# import numpy as np
import tensorflow as tf
# import os

# class written to replicate input_data from
# tensorflow.examples.tutorials.mnist for CIFAR-10
import cifar10_read

# location of the CIFAR-10 dataset
data_dir = '/notebooks/data/cifar-10-batches-py/'

# read in the dataset
print('reading in the CIFAR10 dataset')
dataset = cifar10_read.read_data_sets(
    data_dir,
    distort_train=True,
    one_hot=True,
    reshape=False)

using_tensorboard = True
nbatch = 256
learning_rate = .001
sessionname = "64_64_256_network_with_orig_bn_2"
n_iter = 10000
momentum = 0.9
nf = 64
nf2 = 64

##################################################
# PHASE 1  - ASSEMBLE THE GRAPH

# 1.1) define the placeholders for the input data and the ground truth labels

# x_input can handle an arbitrary number of input
# vectors of length input_dim = d
# y_  are the labels (each label is a length 10 one-hot encoding)
# of the inputs in x_input
# If x_input has shape [N, input_dim] then y_ will have shape [N, 10]

input_dim = 32 * 32 * 3    # d
x_input = tf.placeholder(tf.float32, shape=[None, 32, 32, 3])
y_ = tf.placeholder(tf.float32, shape=[None, 10])

# 1.2) define the parameters of the network
# W: 3072 x 10 weight matrix,  b: bias vector of length 10

is_training = tf.placeholder(tf.float32, [1], 'is_training')

with tf.variable_scope("layer1"):

    W1 = tf.get_variable('w1', [5, 5, 3, nf],
                         initializer=tf.keras.initializers.he_normal())
    b1 = tf.get_variable('b1', [nf],
                         initializer=tf.constant_initializer(0.1))

    y1_ = tf.nn.conv2d(x_input, W1,
                       strides=[1, 1, 1, 1], padding='SAME') + b1

    y1 = tf.nn.relu(y1_)

    # compute the mean and variance of the responses in the batch
    shape = y1.get_shape().as_list()

    # create Variables learned by the network used by batch normalization
    gamma = tf.Variable(tf.ones(shape[-1]))
    beta = tf.Variable(tf.zeros(shape[-1]))

    # create Variables not trained by the optimizer class
    # to keep track of the population mean and variance
    pop_mean = tf.Variable(tf.zeros(shape[-1]), trainable=False)
    pop_var = tf.Variable(tf.ones(shape[-1]), trainable=False)

    batch_mean, batch_var = tf.nn.moments(y1, range(len(shape) - 1))

    decay = .9 * is_training[0] + 1 * (1 - is_training[0])
    train_mean = tf.assign(pop_mean, pop_mean * decay +
                           batch_mean * (1 - decay))
    train_var = tf.assign(pop_var, pop_var * decay + batch_var * (1 - decay))

    # assign the values to pop mean and pop var before the following ops
    with tf.control_dependencies([train_mean, train_var]):
        # Compute the mean and var to use in bn op, depends on is training[0]
        u_mean = batch_mean * is_training[0] + pop_mean * (1 - is_training[0])
        u_var = batch_var * is_training[0] + pop_var * (1 - is_training[0])

        bn1 = tf.nn.batch_normalization(y1, u_mean, u_var, beta, gamma, 10e-8)

    # bn1 = tf.contrib.layers.batch_norm(y1, scale=True,
    #                                   is_training=is_training[0], scope='bn')


with tf.variable_scope("layer1_maxpool"):
    H1 = tf.nn.max_pool(
        bn1,
        ksize=[1, 3, 3, 1],
        strides=[1, 2, 2, 1],
        padding='SAME')

with tf.variable_scope("layer2"):

    W2 = tf.get_variable('w2', [5, 5, nf, nf2],
                         initializer=tf.keras.initializers.he_normal())
    b2 = tf.get_variable('b2', [nf2],
                         initializer=tf.constant_initializer(0.1))

    y2_ = tf.nn.conv2d(H1, W2,
                       strides=[1, 1, 1, 1], padding='SAME') + b2

    y2 = tf.nn.relu(y2_)

        # compute the mean and variance of the responses in the batch
    shape2 = y2.get_shape().as_list()

    # create Variables learned by the network used by batch normalization
    gamma2 = tf.Variable(tf.ones(shape2[-1]))
    beta2 = tf.Variable(tf.zeros(shape2[-1]))

    # create Variables not trained by the optimizer class
    # to keep track of the population mean and variance
    pop_mean2 = tf.Variable(tf.zeros(shape2[-1]), trainable=False)
    pop_var2 = tf.Variable(tf.ones(shape2[-1]), trainable=False)

    batch_mean2, batch_var2 = tf.nn.moments(y2, range(len(shape2) - 1))

    decay2 = .9 * is_training[0] + 1 * (1 - is_training[0])
    train_mean2 = tf.assign(pop_mean2, pop_mean2 * decay2 +
                           batch_mean2 * (1 - decay2))
    train_var2 = tf.assign(pop_var2, pop_var2 * decay2 + batch_var2 * (1 - decay2))

    # assign the values to pop mean and pop var before the following ops
    with tf.control_dependencies([train_mean2, train_var2]):
        # Compute the mean and var to use in bn op, depends on is training[0]
        u_mean2 = batch_mean2 * is_training[0] + pop_mean * (1 - is_training[0])
        u_var2 = batch_var2 * is_training[0] + pop_var * (1 - is_training[0])

        bn1 = tf.nn.batch_normalization(y1, u_mean, u_var, beta, gamma, 10e-8)


    #bn2 = tf.contrib.layers.batch_norm(y2, scale=True,
    #                                   is_training=is_training[0], scope='bn')


with tf.variable_scope("layer2_maxpool"):
    H2 = tf.nn.max_pool(
        y2,
        ksize=[1, 3, 3, 1],
        strides=[1, 2, 2, 1],
        padding='SAME')

    y2_flat = tf.reshape(H2, [-1, 8 * 8 * nf2])

# Fully connected layer
with tf.variable_scope("layer3"):

    W3 = tf.get_variable('w3', [8 * 8 * nf2, 128],
                         initializer=tf.keras.initializers.he_normal())
    b3 = tf.get_variable('b3', [128],
                         initializer=tf.constant_initializer(0.1))

    y3_ = tf.matmul(y2_flat, W3) + b3

    y3 = tf.nn.relu(y3_)


    # bn3 = tf.contrib.layers.batch_norm(y3, scale=True,
    #                                   is_training=is_training[0], scope='bn')


with tf.variable_scope("output"):

    W4 = tf.get_variable('w4', [128, 10],
                         initializer=tf.contrib.layers.xavier_initializer())
    b4 = tf.get_variable('b4', [10],
                         initializer=tf.constant_initializer(0.1))

    y = tf.matmul(y3, W4) + b4

# 1.4) define the loss funtion
# cross entropy loss:
# Apply softmax to each output vector in y to give probabilities for
# each class then compare to the ground truth labels via the cross-entropy
# loss and then compute the average loss over all the input examples
cross_entropy = tf.reduce_mean(
    tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))

# 1.5) Define the optimizer used when training the network ie gradient descent
# or some variation.
# Use gradient descent with a learning rate of .01

train_step = (tf.train.AdamOptimizer(
    learning_rate=learning_rate,
    beta1=0.9,
    beta2=0.999,
    epsilon=1e-08,
    use_locking=False)
    .minimize(cross_entropy))

# (optional) definiton of performance measures
# definition of accuracy, count the number of correct predictions where
# the predictions are made by choosing the class with highest score
correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# 1.6) Add an op to initialize the variables.
init = tf.global_variables_initializer()

##################################################


# If using TENSORBOARD
if using_tensorboard:
    # keep track of the loss and accuracy for the training set
    tf.summary.scalar('training loss', cross_entropy, collections=['training'])
    tf.summary.scalar('training accuracy', accuracy, collections=['training'])
    # merge the two quantities
    tsummary = tf.summary.merge_all('training')

    # keep track of the loss and accuracy for the validation set
    tf.summary.scalar('validation loss', cross_entropy,
                      collections=['validation'])
    tf.summary.scalar('validation accuracy', accuracy,
                      collections=['validation'])
    # merge the two quantities
    vsummary = tf.summary.merge_all('validation')

##################################################


##################################################
# PHASE 2  - PERFORM COMPUTATIONS ON THE GRAPH

# 2.1) start a tensorflow session
with tf.Session() as sess:

    ##################################################
    # If using TENSORBOARD
    if using_tensorboard:
        # set up a file writer and directory to where it should write info +
        # attach the assembled graph
        summary_writer = tf.summary.FileWriter(
            './logs/network5/{}'.format(sessionname),
            sess.graph)

    ##################################################

    # 2.2)  Initialize the network's parameter variables
    # Run the "init" op (do this when training from a random initialization)
    sess.run(init)

    # 2.3) loop for the mini-batch training of the network's parameters
    for i in range(n_iter):

        # grab a random batch (size nbatch) of labelled training examples
        batch = dataset.train.next_batch(nbatch)

        # create a dictionary with the batch data
        # batch data will be fed to the placeholders for inputs
        # "x_input" and labels "y_"
        batch_dict = {
            x_input: batch[0],  # input data
            y_: batch[1],  # corresponding labels
            "is_training:0": [1.0]
        }

        # run an update step of mini-batch by calling the "train_step" op
        # with the mini-batch data. The network's parameters will be updated
        # after applying this operation
        sess.run(train_step, feed_dict=batch_dict)

        # periodically evaluate how well training is going
        if i % 100 == 0:

            # compute the performance measures on the training set by
            # calling the "cross_entropy" loss and "accuracy" ops with the
            # training data fed to the placeholders "x_input" and "y_"

            batch_dict['is_training:0'] = [0.0]
            tr = sess.run([cross_entropy, accuracy],
                          feed_dict=batch_dict)

            # compute the performance measures on the validation set by
            # calling the "cross_entropy" loss and "accuracy" ops with the
            # validation data fed to the placeholders "x_input" and "y_"

            val = sess.run([cross_entropy, accuracy],
                           feed_dict={x_input: dataset.validation.images,
                                      y_: dataset.validation.labels,
                                      is_training: [0.0]})

            info = [i] + tr + val
            print(info)

            ##################################################
            # If using TENSORBOARD
            if using_tensorboard:

                # compute the summary statistics and write to file
                summary_str = sess.run(tsummary,
                                       feed_dict=batch_dict)
                summary_writer.add_summary(summary_str, i)

                summary_str1 = sess.run(vsummary, feed_dict={
                                        x_input: dataset.validation.images,
                                        y_: dataset.validation.labels,
                                        "is_training:0": [0.0]})

                summary_writer.add_summary(summary_str1, i)
            ##################################################

    # evaluate the accuracy of the final model on the test data
    test_acc = sess.run(accuracy,
                        feed_dict={
                            x_input: dataset.test.images,
                            y_: dataset.test.labels,
                            "is_training:0": [0.0]})

    final_msg = 'test accuracy:' + str(test_acc)
    print(final_msg)

##################################################
