import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import tensorflow as tf
from hyperopt import fmin, tpe, hp, STATUS_OK, Trials
# from hyperopt.mongoexp import MongoTrials
# import horovod.tensorflow as hvd

import cifar_data
import cifar10_read
import cnn_models_old
import util

import psutil
from pushover import Client
from time import time
import numpy as np
import json as json
import copy

def my_decay(learning_rate, global_step):
    return tf.train.exponential_decay(
        learning_rate,
        global_step,
        500,
        0.96,
        staircase=True)


def model_fn(features, labels, mode, params):

    y = cnn_models_old.vgg16(features, mode, params)
    y_ = labels

    with tf.variable_scope("evaluation"):
        with tf.variable_scope("prediction"):
            # scores = tf.nn.softmax(y, name='softmax_tensor')
            pred = tf.argmax(y, 1)
            # prediction = tf.reshape(y, [-1])

    predictions = {
        'results': pred,
        'probabilities': tf.nn.softmax(y, name="softmax_tensor"),
        'logits': y,
    }

    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(
            mode=mode,
            predictions=predictions)

    with tf.variable_scope("cross_entropy"):
        cross_entropy = tf.nn.softmax_cross_entropy_with_logits_v2(
            labels=y_,
            logits=y)

        loss = tf.reduce_mean(cross_entropy, name='xentropy_mean') + \
            tf.losses.get_regularization_loss()

    with tf.variable_scope("accuracy"):
        accuracy_op = tf.metrics.accuracy(
            labels=tf.argmax(y_, 1), predictions=pred)
        eval_metric = {'accuracy': accuracy_op}

        # correct_prediction = tf.equal(pred, tf.argmax(y_, 1))
        # accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    eval_metric = {'accuracy': accuracy_op}

    # summary_hook = tf.train.SummarySaverHook(
    #     save_steps=10,
    #     summary_op=tf.summary.merge_all())

    if mode == tf.estimator.ModeKeys.EVAL:

        return tf.estimator.EstimatorSpec(
            mode=mode,
            loss=loss,
            # predictions=predictions,
            eval_metric_ops=eval_metric)  # ,
        #    evaluation_hooks=[summary_hook])

    if mode == tf.estimator.ModeKeys.TRAIN:

        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):

            # Horovod: scale learning rate by the number of workers.

            train_op = tf.contrib.layers.optimize_loss(
                loss=loss,
                global_step=tf.train.get_global_step(),
                optimizer=tf.train.AdamOptimizer,
                learning_rate=10**params.learning_rate,  # * hvd.size(),
                learning_rate_decay_fn=my_decay
            )

        # optimizer = tf.train.AdamOptimizer(
        #     learning_rate=0.001 * hvd.size())

        # # Horovod: add Horovod Distributed Optimizer.
        # optimizer = hvd.DistributedOptimizer(optimizer)

        # train_op = optimizer.minimize(
        #     loss=loss,
        #     global_step=tf.train.get_global_step())

        # hooks = [summary_hook]  # if hvd.rank() == 0 else None

        return tf.estimator.EstimatorSpec(
            mode=mode,
            loss=loss,
            train_op=train_op)  # ,
        # training_hooks=hooks)
    else:
        train_op = None


def objective(param):

    # hvd.init()

    logger = util.get_logger("objective")

    hparams = tf.contrib.training.HParams(
        dropout=param['dropout'],
        keep_prob=param['keep_prob'],
        learning_rate=param['learning_rate'],
        hidden_units=param['hidden_units'],
        lmbda=param['lmbda'])

    # Should not be set here

    logbasedir = param['logbasedir']

    path = "{}_{}_{}_{}_{}".format(
        logbasedir,
        hparams.learning_rate,
        hparams.keep_prob,
        hparams.hidden_units,
        hparams.lmbda)  # if hvd.rank() == 0 else None

    logger.info("Starting trial: {}".format(path))

    # Create a new directory for each try
    # so that we get nice graphs in Tensorboard

    # config = tf.ConfigProto(
    #     allow_soft_placement=True,
    #     log_device_placement=True)
    # config.gpu_options.allow_growth = True
    # config.gpu_options.visible_device_list = str(hvd.local_rank())

    # logger.debug("Horovod visible devices: {}".format(str(hvd.local_rank())))

    # bcast_hook = hvd.BroadcastGlobalVariablesHook(0)

    eval_input_fn = cifar_data.input_fn(
        False, data_dir, batchsize, num_epochs=1,
        num_parallel_calls=no_parallel_calls,
    )

    train_input_fn = cifar_data.input_fn(
        True, data_dir, batchsize, num_epochs=epochs,
        num_parallel_calls=no_parallel_calls)

    run_config = tf.estimator.RunConfig(
        model_dir=path,
        save_checkpoints_steps=10000,
        save_summary_steps=1000)
    # ,
    #    session_config=config)

    train_spec = tf.estimator.TrainSpec(
        input_fn=train_input_fn,
        max_steps=param['max_steps'])  # // hvd.size(),
    # hooks=[bcast_hook])

    # exporter = tf.estimator.LatestExporter('exporter', serving_input_fn)

    # Start at 0 seconds, evaluate every 2 min
    eval_spec = tf.estimator.EvalSpec(
        input_fn=eval_input_fn,
        start_delay_secs=0,
        throttle_secs=60)

    estimator = tf.estimator.Estimator(
        model_fn=model_fn,  # First-class function
        params=hparams,  # HParams
        config=run_config  # RunConfig
    )

    tf.estimator.train_and_evaluate(estimator, train_spec, eval_spec)

    eval_results = estimator.evaluate(input_fn=eval_input_fn)

    message = ("A training run {} has been completed "
               "with loss {} and accuracy {}%."
               .format(
                   path,
                   round(eval_results['loss'], 2),
                   round(100 * eval_results['accuracy'], 2)))

    logger.info(message)
    client.send_message(message, title="Tensorflow complete!")

    results = {
               "input": param,
               "batchsize": batchsize,
               "accuracy": float(eval_results['accuracy']),
               "loss": float(eval_results['loss']),
               "step": float(eval_results['global_step']),
               "path": path
            }

    str_results = json.dumps(results)

    with open("results.json", "a") as myfile:
        myfile.write(str_results)

    return {
        'loss': -eval_results['accuracy'],
        'status': STATUS_OK,
        'eval_time': time()
    }


if __name__ == "__main__":
    # tf.app-run()

    # hvd.init()
    tf.logging.set_verbosity(tf.logging.INFO)
    # Probably not great to put them here.
    logbasedir = '../logs/network6/ensemble/'

    max_evals = 30
    augmentation = True
    batchsize = 128
    max_steps = 50000
    max_steps_final = 100000
    epochs = max_steps / (50000 / batchsize)
    number_of_trials = 10

    no_parallel_calls = psutil.cpu_count()

    # https://gist.github.com/alsrgv/34a32f30292f4e2c1fa29ec0d65dea26

    logger = util.get_logger("Main")

    config = util.read_config("../config.json")

    client = Client(config["user_key"], api_token=config["token"])

    client.send_message(
        "A new training run has started.",
        title="Starting Tensorflow")

    with tf.variable_scope("hyperparameters"):
        keep_prob = tf.placeholder(tf.float32)
        global_step = tf.train.create_global_step()

    tensors_to_log = {
        "cross_entropy_loss": "loss",
        "accuracy": "accuracy_op"
    }

    logging_hook = tf.train.LoggingTensorHook(
        tensors=tensors_to_log, every_n_iter=50)

    data_dir = '/notebooks/data/'
    logger.info('Reading in the CIFAR10 dataset from {}'.format(data_dir))

    # ,
    # num_workers=hvd.size(),
    # worker_index=hvd.rank())

    test_input_fn = cifar_data.input_fn(
        False, data_dir, batchsize, num_epochs=1,
        num_parallel_calls=no_parallel_calls)

    # params = {
    #     'dropout': True,
    #     'lmbda': hp.quniform('lmbda', -7, -5, 1),
    #     'keep_prob': hp.quniform('keep_prob', .75, .9, .025),
    #     'hidden_units': hp.quniform('hidden_units', 64, 1024, 64),
    #     'learning_rate': hp.quniform('learning_rate', -3, -2, 1),
    #     'max_steps': max_steps,
    #     'logbasedir': logbasedir
    # }

    # trials = Trials()
    # # MongoTrials('mongo://mongodb:27017/foo_db/jobs', exp_key='exp1')
    # best = fmin(objective, params, algo=tpe.suggest,
    #             trials=trials, max_evals=max_evals)

    # message = "A training run has been completed with best result {}".format(
    #     best)

    # logger.info(message)
    # client.send_message(message, title="Tensorflow complete!")

    best = {'learning_rate': -2.0, 'keep_prob': 0.8,
            'hidden_units': 384.0, 'lmbda': -5.0}

    model_path = "{}_{learning_rate}_{keep_prob}_{hidden_units}_{lmbda}/".format(
         logbasedir, **best)

    # run_config = tf.estimator.RunConfig(
    #     model_dir=model_path,
    #     save_checkpoints_steps=10000,
    #     save_summary_steps=1000)

    # hparams = tf.contrib.training.HParams(
    #     dropout=True,
    #     keep_prob=best['keep_prob'],
    #     learning_rate=best['learning_rate'],
    #     hidden_units=best['hidden_units'],
    #     lmbda=best['lmbda'])

    eval_input_fn = cifar_data.input_fn(
        False, data_dir, batchsize, num_epochs=1,
        num_parallel_calls=no_parallel_calls,
    )

    train_input_fn = cifar_data.input_fn(
        True, data_dir, batchsize, num_epochs=epochs,
        num_parallel_calls=no_parallel_calls)

    # eval_spec = tf.estimator.EvalSpec(
    #     input_fn=eval_input_fn,
    #     start_delay_secs=0,
    #     throttle_secs=120)

    # estimator = tf.estimator.Estimator(
    #     model_fn=model_fn,  # First-class function
    #     params=hparams,  # HParams
    #     config=run_config  # RunConfig
    # )

    # eval_results = estimator.evaluate(input_fn=test_input_fn)

    # message = "Accuracy on test set: {} %".format(eval_results['accuracy'])
    # logger.info(message)
    # client.send_message(message, title="Tensorflow complete!")

    # p = list(estimator.predict(input_fn=test_input_fn))
    # classes = [a['results'] for a in p]

    # logger.debug(util.ppjson(best))

    i = 1
    model_path = ("{}ensemble_{}/_{learning_rate}_{keep_prob}_{hidden_units}_{lmbda}"
                  .format(
                      logbasedir, i + 1, **best))

    # model_path = "./logs/network6/_0.00162663759893_0.9_192.0/eval"

    # best = {'hidden_units': 192.0, 'keep_prob': 0.9,
    #        'learning_rate': 0.00162663759893}

    a = []
    # number_of_trials = 10

    # More epochs!
    # train_input_fn = tf.estimator.inputs.numpy_input_fn(
    #     x={"x": train_data},
    #     y=train_labels,
    #     batch_size=batchsize,
    #     num_epochs=6,
    #     shuffle=True)

    # Number of test samples
    predictions = np.zeros((number_of_trials, 10000))

    for i in range(number_of_trials):
        # Run the experiment

        model_path = "{}ensemble_{}/_{learning_rate}_{keep_prob}_{hidden_units}".format(
            logbasedir, i + 1, **best)

        logger.info("Starting trial: {}".format(model_path))

        run_config = tf.estimator.RunConfig(
            model_dir=model_path,
            save_checkpoints_steps=1000,
            save_summary_steps=200)

        hparams = tf.contrib.training.HParams(
            dropout=True,
            keep_prob=best['keep_prob'],
            learning_rate=best['learning_rate'],
            hidden_units=best['hidden_units'],
            lmbda=best['lmbda'])

        train_spec = tf.estimator.TrainSpec(
            input_fn=train_input_fn,
            max_steps=max_steps_final)  # // hvd.size(),
        # hooks=[bcast_hook])

        # exporter = tf.estimator.LatestExporter('exporter', serving_input_fn)

        # Start at 0 seconds, evaluate every 2 min
        eval_spec = tf.estimator.EvalSpec(
            input_fn=eval_input_fn,
            start_delay_secs=0,
            throttle_secs=120)

        estimator = tf.estimator.Estimator(
            model_fn=model_fn,  # First-class function
            params=hparams,  # HParams
            config=run_config  # RunConfig
        )

        tf.estimator.train_and_evaluate(estimator, train_spec, eval_spec)

        acc = estimator.evaluate(input_fn=test_input_fn)["accuracy"]
        a.append(acc)

        p = list(estimator.predict(input_fn=test_input_fn))
        predictions[i, :] = [result['results'] for result in p]

        accuracy = "{} +/- {}%".format(round(100 * np.mean(a), 2),
                                       round(100 * 1.96 * np.std(a), 2))
        message = "Accuracy on test set after {} runs: {} ({}%)".format(
            i + 1, accuracy, round(acc * 100, 2))
        client.send_message(message, title="Tensorflow complete!")

    # This is bloody stupid, should read test set labels
    # from cifar_data instead.
    test_fname = "../data/cifar-10-batches-py/test_batch"
    test_images, labels = cifar10_read.read_cifar10(
        test_fname, one_hot=False)
    logger.debug(labels)

    predictions.astype(int)

    axis = 0
    arr = predictions.astype(int)
    u, indices = np.unique(arr, return_inverse=True)
    pred = u[np.argmax(
        np.apply_along_axis(np.bincount, axis, indices.reshape(arr.shape),
                            None, np.max(indices) + 1), axis=axis)]

    accuracy = 100.0 * np.sum(pred == labels) / len(labels)

    # accuracy = "{} +/- {}%".format(np.mean(a),1.96*np.std(a))
    message = "Accuracy on test set after {} runs: {} %".format(
        number_of_trials, accuracy)

    logger.info(message)
    client.send_message(message, title="Tensorflow complete!")
