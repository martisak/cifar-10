# CIFAR-10 Assignment for WASP Autonomous Systems I

## Why should you read this?

This is an example of image classification using Tensorflow Estimator and Dataset APIs. This optimized implementation reaches 91.97% +/- 0.37% (95% CI) accuracy on the test set, which is Top 20 on [Who is the best in CIFAR-10?](http://rodrigob.github.io/are_we_there_yet/build/classification_datasets_results.html#43494641522d3130). Using an ensemble of 25 models we reach 94.49% on the test set which is in the top 3. The aim is to optimize this pipeline more than getting better results.

## Dataset

![CIFAR-10](https://kaggle2.blob.core.windows.net/competitions/kaggle/3649/media/cifar-10.png)

The [CIFAR-10](http://www.cs.toronto.edu/~kriz/cifar.html) dataset is a 6000 32 px x 32 px colour image dataset in 10 classes that is a subset of the 80 million tiny images dataset.

See [
CIFAR-10 - Object Recognition in Images](https://www.kaggle.com/c/cifar-10) and [Who is the best in CIFAR-10?](http://rodrigob.github.io/are_we_there_yet/build/classification_datasets_results.html#43494641522d3130)

## Exercises

Description and instructions can be found in [Image Classification with Convolutional Networks & Tensorflow](https://git.cf.ericsson.net/eisamar/cifar-10/blob/master/wasp_tutorial2.pdf).

- [X] Exercise 1: Your turn - Have you grasped the simple stuff?
    + See [`example0.py`](https://git.cf.ericsson.net/eisamar/cifar-10/blob/master/example0.py)   
    + The result should be $$(x*y)**(x+y) = 7776$$.

![Exe+ See [`example0.py`](https://git.cf.ericsson.net/eisamar/cifar-10/blob/master/example0.py)rcise 1 graph](./pictures/exercise1.png)

- [X] Exercise 2: Playing around with training your first network
    +  See [`network1.py`](https://git.cf.ericsson.net/eisamar/cifar-10/blob/master/network1.py) and [`cifar10_read.py`](https://git.cf.ericsson.net/eisamar/cifar-10/blob/master/cifar10_read.py).
    +  Results are saved and viewed in [Tensorboard](kds-gpu-02.rnd.ki.sw.ericsson.se:6007).
    +  Change learning rate to a low (.0001), then high (.1).
    +  Change batch size to low and high. Check speed and stability.
    +  Train for longer. Does the model overfit?
    
- [X] Exercise 3: Implement a 2-layer network with momentum training
    +  See [`network2.py`](https://git.cf.ericsson.net/eisamar/cifar-10/blob/master/network1.py) and [`cifar10_read.py`](https://git.cf.ericsson.net/eisamar/cifar-10/blob/master/cifar10_read.py).
    +  Is it possible to overfit this model?
- [X] Exercise 4: Implementation of a convolutional network
    +  See [`network3.py`](https://git.cf.ericsson.net/eisamar/cifar-10/blob/master/network1.py) and [`cifar10_read.py`](https://git.cf.ericsson.net/eisamar/cifar-10/blob/master/cifar10_read.py).

### Extra material, for advanced students

- [X] Exercise 5: Train your first convolutional network on a GPU
    + All of these exercises have been done on `kds-gpu-02.rnd.ki.sw.ericsson.se`.
- [X] Exercise 6: Take advantage of data-augmentation
    +  See [`network4.py`](https://git.cf.ericsson.net/eisamar/cifar-10/blob/master/network1.py) and [`cifar10_read.py`](https://git.cf.ericsson.net/eisamar/cifar-10/blob/master/cifar10_read.py).
- [X] Exercise 7: Add a second convolutional layer to your network
    +  See [`network5.py`](https://git.cf.ericsson.net/eisamar/cifar-10/blob/master/network1.py) and [`cifar10_read.py`](https://git.cf.ericsson.net/eisamar/cifar-10/blob/master/cifar10_read.py).
- [X] Exercise 8: Apply batch normalization
    +  Batch-normalization using `tf.contrib.layers.batch_norm`, doesn't really work. See [`network5_bn.py`](https://git.cf.ericsson.net/eisamar/cifar-10/blob/master/network1.py) and [`cifar10_read.py`](https://git.cf.ericsson.net/eisamar/cifar-10/blob/master/cifar10_read.py).
    + Batch-normalization on first layer, using `tf.nn.batch_normalization`. See [`network5_bn_orig.py`](https://git.cf.ericsson.net/eisamar/cifar-10/blob/master/network1.py) and [`cifar10_read.py`](https://git.cf.ericsson.net/eisamar/cifar-10/blob/master/cifar10_read.py).
    + Batch-normalization on both layers, using `tf.nn.batch_normalization`. See [`network5_bn_orig_2.py`](https://git.cf.ericsson.net/eisamar/cifar-10/blob/master/network1.py) and [`cifar10_read.py`](https://git.cf.ericsson.net/eisamar/cifar-10/blob/master/cifar10_read.py).

### Getting better results

Two models have been tried.

A VGG16-like (Simonyan, Zisserman, 2015) model with Dropout / Drop Connect and batch normalization. See [`network6.py`](https://git.cf.ericsson.net/eisamar/cifar-10/blob/master/exercises/network6.py), [`cifar_data.py`](https://git.cf.ericsson.net/eisamar/cifar-10/blob/master/exercisescifar10_read.py) and [`cnn_models.py`](https://git.cf.ericsson.net/eisamar/cifar-10/blob/master/exercises/cifar10_read.py). Dropout was used on some layers. Drop-connect was also tried.

An [all-convolutional model](http://arxiv.org/pdf/1412.6806.pdf) (Springenberg et.al., 2015), see [`network6_all_cnn.py`](https://git.cf.ericsson.net/eisamar/cifar-10/blob/master/exercises/network6_all_cnn.py). Batch normalization was used after each "pooling"-layer. Dropout and L2 regularization was used.

These files are completely different from the exercise examples and uses `tf.estimator` and `tf.data.Dataset`. This was important to speed up learning.

conv3-64 here means 64 3x3 kernels.

| Layer |   VGG16   |      All-CNN       |
|-------|-----------|--------------------|
|     1 | conv3-64  | conv3-96           |
|     2 | conv3-64  | conv3-96           |
|     3 | max-pool2 | conv3-96 stride 2  |
|     4 | conv3-128 | conv3-196          |
|     5 | conv3-128 | conv3-196          |
|     6 | max-pool2 | conv3-196 stride 2 |
|     7 | conv3-256 | dropout            |
|     8 | conv3-256 |                    |
|     9 | max-pool2 |                    |
|    10 | conv3-512 |                    |
|    11 | conv3-512 |                    |
|    12 | conv3-512 |                    |
|    13 | max-pool2 |                    |
|    14 | dense     |                    |
|    15 | dense     | dense              |
    

#### Data augmentation

Implemented in `preprocess_image(image, is_training)`.

* Resizing image to 40 px x 40 px.
* Random rotation between -10 and 10 degrees.
* Random crop down to 32 px x 32 px.
* Random flip (left/right)
* Per image standardization.

## Installation and getting started

This has been tested on `kds-gpu-02.rnd.ki.sw.ericsson.se`.

1. Install [nvidia-docker-compose](https://github.com/eywalker/nvidia-docker-compose) on the machine with 

    ```
    pip install --user nvidia-docker-compose
    ```

**Optional** since pipeline now use standard `docker-compose` after setting `default_runtime: 'nvidia'` in `/etc/docker/daemon.json`.

2. Clone this repo 

    ```
    git clone git@gitlab.com:martisak/cifar-10.git
    ```

3. Download [CIFAR-10](https://www.cs.toronto.edu/~kriz/cifar.html) (binary version is used in some files, and python-version in the exercises) into subdirectory `./data`. 

    ```
    make data
    ```

4. Check how devices are connected and pick the ones to use. Enter these into `docker-compose.yml`.

    ```
    eisamar@kds-gpu-02: ~
    $ nvidia-smi topo --matrix
        GPU0    GPU1    GPU2    GPU3    GPU4    GPU5    GPU6    GPU7    CPU Affinity
    GPU0     X  PIX PHB PHB SYS SYS SYS SYS 0-11,24-35
    GPU1    PIX  X  PHB PHB SYS SYS SYS SYS 0-11,24-35
    GPU2    PHB PHB  X  PIX SYS SYS SYS SYS 0-11,24-35
    GPU3    PHB PHB PIX  X  SYS SYS SYS SYS 0-11,24-35
    GPU4    SYS SYS SYS SYS  X  PIX PHB PHB 12-23,36-47
    GPU5    SYS SYS SYS SYS PIX  X  PHB PHB 12-23,36-47
    GPU6    SYS SYS SYS SYS PHB PHB  X  PIX 12-23,36-47
    GPU7    SYS SYS SYS SYS PHB PHB PIX  X  12-23,36-47
    
    Legend:
    
      X    = Self
      SYS  = Connection traversing PCIe as well as the SMP interconnect between NUMA nodes (e.g., QPI/UPI)
      NODE = Connection traversing PCIe as well as the interconnect between PCIe Host Bridges within a NUMA node
      PHB  = Connection traversing PCIe as well as a PCIe Host Bridge (typically the CPU)
      PXB  = Connection traversing multiple PCIe switches (without traversing the PCIe Host Bridge)
      PIX  = Connection traversing a single PCIe switch
      NV#  = Connection traversing a bonded set of # NVLinks
    ```


5. Start Tensorflow and Tensorboard containers `make start`. This uses `docker-compose.yml` start up two containers. Check configuration in `Makefile`. 

    ```
    TENSORBOARD_PORT=6007
    TENSORFLOW_PORT=3001
    COMPOSE_PROJECT_NAME=eisamar_cifar
    TEMPLATE=docker-compose.yml.jinja
    HOST=gpu2
    NV_GPU='6'
    ```

6. Log into the Tensorflow container 

    ```
    docker exec eisamarcifar_tensorflow_1 bash
    ```

7. Check that the NVIDIA cards are detected.

    ```
    root@66ab8b6cc1cb:/notebooks# nvidia-smi
    Thu Mar  1 14:29:24 2018
    +-----------------------------------------------------------------------------+
    | NVIDIA-SMI 390.30                 Driver Version: 390.30                    |
    |-------------------------------+----------------------+----------------------+
    | GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
    | Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
    |===============================+======================+======================|
    |   0  GeForce GTX 1080    Off  | 00000000:04:00.0 Off |                  N/A |
    | 27%   40C    P2   111W / 180W |   4435MiB /  8119MiB |     31%      Default |
    +-------------------------------+----------------------+----------------------+
    |   1  GeForce GTX 1080    Off  | 00000000:85:00.0 Off |                  N/A |
    | 27%   28C    P8    10W / 180W |     18MiB /  8119MiB |      0%      Default |
    +-------------------------------+----------------------+----------------------+
    |   2  GeForce GTX 1080    Off  | 00000000:89:00.0 Off |                  N/A |
    | 27%   24C    P8    10W / 180W |     18MiB /  8119MiB |      0%      Default |
    +-------------------------------+----------------------+----------------------+
    
    +-----------------------------------------------------------------------------+
    | Processes:                                                       GPU Memory |
    |  GPU       PID   Type   Process name                             Usage      |
    |=============================================================================|
    +-----------------------------------------------------------------------------+
    ```


8. Install dependencies in the container.

    ```
    pip install -r requirements.txt
    ```

9. Set [Pushover](https://pushover.net/) parameters in `config.json`.

    ```
    {
        "user_key": "a_user_key",
        "token": "a_token"
    }
    ```

10. Run with `CUDA_VISIBLE_DEVICES=0 python network6.py`.

11. Check [Tensorboard](http://kds-gpu-02.rnd.ki.sw.ericsson.se:6007).

## Further work

- [X] Implement using `tf.estimator.TrainSpec` etc.
- [ ] Hyper-parameter tuning (on multiple GPUs)
- [ ] Single model, multiple GPU
- [X] Getting into Kaggle Top-10.
- [ ] ~~Why is `global_step/sec` decreasing?~~ (it isn't anymore)
- [X] Larger model
- [ ] Use `tf.app.flags`
- [ ] Preprocessing (rotate, flip) as hyperparameters
- [ ] [Parallelize hyperopt evaluations](https://github.com/hyperopt/hyperopt/wiki/Parallelizing-Evaluations-During-Search-via-MongoDB)

## Acknowledgments

This work was partially supported by the Wallenberg AI, Autonomous Systems and Software Program (WASP) funded by the Knut and Alice Wallenberg Foundation.
